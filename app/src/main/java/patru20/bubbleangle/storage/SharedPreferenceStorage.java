package patru20.bubbleangle.storage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import patru20.bubbleangle.orientation.OrientationManager;

public class SharedPreferenceStorage {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private static SharedPreferenceStorage sharedPreferenceStorage;

    public static SharedPreferenceStorage getInstance(){
        if(sharedPreferenceStorage == null){
            sharedPreferenceStorage = new SharedPreferenceStorage();
        }
        return sharedPreferenceStorage;
    }

    @SuppressLint("CommitPrefEdits")
    public void initialize(Context context){

        this.prefs = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        editor = prefs.edit(); // Returns a new instance of the {@link Editor} interface, allowing you to modify the values
    }

    public void addSettingsPreferences(SettingKeys enumKey, Object value){

        String key = enumKey.name();

        if(value instanceof Boolean){
            editor.putBoolean(key, (boolean) value);
        }

        editor.apply();
    }

    public boolean getBooleanPreferences(SettingKeys key, boolean defaultValue){
        return prefs.getBoolean(key.name(), defaultValue);
    }

    public double getDoublePreferences(CalibrationKeys key, double defaultValue){
        return prefs.getFloat(key.name(), (float) defaultValue);
    }

    private OrientationManager orientationManager = OrientationManager.getInstance();

    public void addCalibrationPreferences(float[] angles){

        switch (orientationManager.getOrientation()){
            case FACE_UP:
                editor.putFloat(CalibrationKeys.FACE_UP_CALIBRATED_X.name(), angles[0]);
                editor.putFloat(CalibrationKeys.FACE_UP_CALIBRATED_Y.name(), angles[1]);
                break;
            case FACE_DOWN:
                editor.putFloat(CalibrationKeys.FACE_DOWN_CALIBRATED_X.name(), angles[0]);
                editor.putFloat(CalibrationKeys.FACE_DOWN_CALIBRATED_Y.name(), angles[1]);
                break;
            case PORTRAIT_DOWN:
                editor.putFloat(CalibrationKeys.PORTRAIT_DOWN_CALIBRATED.name(), angles[0]);
                break;
            case PORTRAIT_UP:
                editor.putFloat(CalibrationKeys.PORTRAIT_UP_CALIBRATED.name(), angles[0]);
                break;
            case LANDSCAPE_LEFT:
                editor.putFloat(CalibrationKeys.LANDSCAPE_LEFT_CALIBRATED.name(), angles[0]);
                break;
            case LANDSCAPE_RIGHT:
                editor.putFloat(CalibrationKeys.LANDSCAPE_RIGHT_CALIBRATED.name(), angles[0]);
                break;
        }
        editor.apply();
    }

    public void resetSharedPreferencesCalibration(){

        for (CalibrationKeys calibrationKey: CalibrationKeys.values()) {
            resetCalibration(calibrationKey);
        }
        editor.apply();
    }

    private void resetCalibration(CalibrationKeys calibrationKeys){
        editor.putFloat(calibrationKeys.name(), 0);
    }

    public enum SettingKeys{
        BACKGROUND,
        SCREEN_ON,
        SOUNDS,
        PERMISSION_GRANTED
    }

    public enum CalibrationKeys{
        FACE_UP_CALIBRATED_X,
        FACE_UP_CALIBRATED_Y,
        FACE_DOWN_CALIBRATED_X,
        FACE_DOWN_CALIBRATED_Y,
        PORTRAIT_UP_CALIBRATED,
        PORTRAIT_DOWN_CALIBRATED,
        LANDSCAPE_LEFT_CALIBRATED,
        LANDSCAPE_RIGHT_CALIBRATED
    }
}
