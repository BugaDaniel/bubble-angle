package patru20.bubbleangle;

import android.app.Application;

import patru20.bubbleangle.angle.AccelerometerHandler;
import patru20.bubbleangle.storage.SharedPreferenceStorage;

public class BubbleAngleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AccelerometerHandler.getInstance().initialize(this);
        SharedPreferenceStorage.getInstance().initialize(this);
    }
}
