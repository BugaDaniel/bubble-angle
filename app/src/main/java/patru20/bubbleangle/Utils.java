package patru20.bubbleangle;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class Utils{

    public static int convertDpiToPx(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static void showToast(Context context, String message){
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM,0, Utils.convertDpiToPx(context, 20));
        toast.show();
    }
}
