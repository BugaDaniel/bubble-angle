package patru20.bubbleangle.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import patru20.bubbleangle.Utils;
import patru20.bubbleangle.activities.utils.TransparentBackground;
import patru20.bubbleangle.angle.AccelerometerHandler;
import patru20.bubbleangle.angle.CalibrationHandler;
import patru20.bubbleangle.fragments.core.OrientationFragmentManager;
import patru20.bubbleangle.orientation.OrientationManager;
import patru20.bubbleangle.R;
import patru20.bubbleangle.storage.SharedPreferenceStorage;

public class MainActivity extends AppCompatActivity implements CalibrationHandler.CalibrationCompleteCallback {

    private SurfaceView surfaceView;
    private TransparentBackground transparentBackground;
    private OrientationFragmentManager orientationFragmentManager;
    private FragmentManager fragmentManager;
    private OrientationManager orientationManager;
    private AccelerometerHandler accelerometerHandler;
    private Toolbar toolbar;
    private FrameLayout frameLayout;
    private boolean locked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frameLayout = findViewById(R.id.fragment_placeholder);
        surfaceView = findViewById(R.id.camera_preview);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        transparentBackground = new TransparentBackground(surfaceView, frameLayout);
        fragmentManager = getSupportFragmentManager();
        orientationFragmentManager = new OrientationFragmentManager(fragmentManager);
        accelerometerHandler = AccelerometerHandler.getInstance();
        orientationManager = OrientationManager.getInstance();
        orientationManager.initialize(getWindowManager().getDefaultDisplay());
        orientationManager.registerOrientationChangeListener(orientationFragmentManager);
        orientationManager.registerOrientationChangeListener(transparentBackground);

        AccelerometerHandler.getInstance().registerCalibrationHandler(this);

        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleFreezeScreen();
                showToast(locked);
                updateScreenState();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        boolean keepScreenOn = SharedPreferenceStorage.getInstance().
                getBooleanPreferences(SharedPreferenceStorage.SettingKeys.SCREEN_ON, true);

        if(keepScreenOn){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        else{
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        accelerometerHandler.startAccelerometer();

        if(transparentBackground.isBackgroundEnabled()){

            if(transparentBackground.getPermission()){
                transparentBackground.start();
                locked = !locked;
                toggleFreezeScreen();
            }
        }

        if(OrientationManager.getInstance().getOrientation() != null){
            orientationFragmentManager.onOrientationChanged(OrientationManager.getInstance().getOrientation());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        accelerometerHandler.stopAccelerometer();
        transparentBackground.stop();

        if(locked){
            resetFreezeScreen();
        }

        if(accelerometerHandler.isCalibrationStarted()){
            accelerometerHandler.cancelCalibration();
            updateScreenState();
        }
    }

    private MenuItem calibrationMenuItem;
    private MenuItem lockUnlockMenuItem;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        calibrationMenuItem = toolbar.getMenu().findItem(R.id.calibrate);
        lockUnlockMenuItem = toolbar.getMenu().findItem(R.id.lock_unlock_screen);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.lock_unlock_screen:
                        toggleFreezeScreen();
                        showToast(locked);
                        updateScreenState();
                        break;
                    case R.id.settings:
                        Intent settingsIntent = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(settingsIntent);
                        break;
                    case R.id.calibrate:
                        if(accelerometerHandler.isCalibrationStarted()){
                            accelerometerHandler.cancelCalibration();
                        }
                        else{
                            accelerometerHandler.startCalibration();
                        }
                        updateScreenState();
                }
                return false;
            }
        });
        
        return true;
    }

    private int toolbarIconColor;

    public void setToolbarBackground(boolean transparent){

        int toolbarColor;

        if(transparent){
            toolbarColor = Color.TRANSPARENT;
            toolbarIconColor = Color.WHITE;
        }
        else{
            toolbarColor = Color.WHITE;
            toolbarIconColor = Color.BLACK;
        }

        if(toolbar != null){
            toolbar.setBackgroundColor(toolbarColor);

            if(toolbar.getMenu().findItem(R.id.lock_unlock_screen) != null){
                updateLockUnlockIcon();
                updateToolbarColors(transparent);
            }
        }
    }

    private void toggleFreezeScreen(){

        locked = !locked;

        if(locked){
            accelerometerHandler.stopAccelerometer();
            transparentBackground.pauseCamera();

        }
        else{
            accelerometerHandler.startAccelerometer();
            transparentBackground.resumeCamera();
        }


        updateLockUnlockIcon();
        toggleScreenOrientation(locked);
    }

    private void updateLockUnlockIcon(){

        if(lockUnlockMenuItem == null){
            return;
        }

        int lockUnlockIcon;

        if(!locked){

            if(toolbarIconColor == Color.WHITE){
                lockUnlockIcon = R.drawable.ic_lock_open_white_24dp;
            }
            else {
                lockUnlockIcon = R.drawable.ic_lock_open_black_24dp;
            }
        }
        else{

            if(toolbarIconColor == Color.WHITE){
                lockUnlockIcon = R.drawable.ic_lock_white_24dp;
            }
            else {
                lockUnlockIcon = R.drawable.ic_lock_black_24dp;
            }
        }

        lockUnlockMenuItem.setIcon(lockUnlockIcon);

    }

    enum ScreenState{
        DEFAULT,
        LOCKED,
        CALIBRATING
    }

    private ScreenState getScreenState(){

        if(locked){
            return ScreenState.LOCKED;
        }
        if(accelerometerHandler.isCalibrationStarted()){
            return ScreenState.CALIBRATING;
        }
        return ScreenState.DEFAULT;
    }

    private void updateScreenState(){

        switch (getScreenState()){
            case DEFAULT:
                toggleMenuItem(lockUnlockMenuItem, true);
                toggleMenuItem(calibrationMenuItem, true);

                calibrationMenuItem.getIcon().clearColorFilter();
                frameLayout.setClickable(true);
                break;
            case LOCKED:
                toggleMenuItem(lockUnlockMenuItem, true);
                toggleMenuItem(calibrationMenuItem, false);
                break;
            case CALIBRATING:
                toggleMenuItem(lockUnlockMenuItem, false);

                calibrationMenuItem.getIcon().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
                frameLayout.setClickable(false);
                break;
        }
    }

    private void toggleMenuItem(MenuItem menuItem, boolean enabled){

        if(menuItem != null){
            menuItem.setEnabled(enabled);
            menuItem.getIcon().setAlpha(enabled ? 255 : (int)(0.2 * 255));
        }
    }

    private void updateToolbarColors(boolean isBackgroundTransparent){

        int settingsIcon;
        int calibrateIcon;
        int color;

        if(isBackgroundTransparent){
            settingsIcon = R.drawable.ic_settings_white_24dp;
            calibrateIcon = R.drawable.ic_calibration_white;
            color = Color.WHITE;
        }
        else {
            settingsIcon = R.drawable.ic_settings_black_24dp;
            calibrateIcon = R.drawable.ic_calibration_black;
            color = Color.BLACK;
        }

        toolbar.setTitleTextColor(color);
        toolbar.getMenu().findItem(R.id.settings).setIcon(settingsIcon);
        toolbar.getMenu().findItem(R.id.calibrate).setIcon(calibrateIcon);
        updateScreenState();
    }

    private void showToast(boolean locked){
        Utils.showToast(this, locked ? getResources().getString(R.string.screen_lock) : getResources().getString(R.string.screen_unlock));
    }

    private void toggleScreenOrientation(boolean locked){

        int screenOrientation;

        if(locked){
            screenOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED;
        }
        else{
            screenOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR;
        }

        setRequestedOrientation(screenOrientation);
    }

    private void resetFreezeScreen(){
        locked = false;
        updateLockUnlockIcon();
        toggleScreenOrientation(locked);
    }

    @Override
    public void calibrationComplete() {
        updateScreenState();
    }

}
