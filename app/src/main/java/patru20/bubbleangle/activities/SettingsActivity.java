package patru20.bubbleangle.activities;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import patru20.bubbleangle.R;
import patru20.bubbleangle.activities.utils.TransparentBackground;
import patru20.bubbleangle.angle.AccelerometerHandler;
import patru20.bubbleangle.storage.SharedPreferenceStorage;

public class SettingsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private SwitchCompat backgroundSwitch;
    private SwitchCompat screenOnSwitch;
    private SwitchCompat soundSwitch;
    private TextView resetCalibration;
    private SharedPreferenceStorage sharedPreferenceStorage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);

        backgroundSwitch = findViewById(R.id.background_transparent_setting);
        screenOnSwitch = findViewById(R.id.screen_always_on_setting);
        soundSwitch = findViewById(R.id.sound_enabler);
        resetCalibration = findViewById(R.id.reset_calibration);
        toolbar = findViewById(R.id.settings_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        sharedPreferenceStorage = SharedPreferenceStorage.getInstance();

        backgroundSwitch.setChecked(SharedPreferenceStorage.getInstance().
                getBooleanPreferences(SharedPreferenceStorage.SettingKeys.BACKGROUND, false));

        backgroundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    TransparentBackground.askPermission(SettingsActivity.this);
                }

                sharedPreferenceStorage.addSettingsPreferences(SharedPreferenceStorage.SettingKeys.BACKGROUND, isChecked);
            }
        });

        screenOnSwitch.setChecked(SharedPreferenceStorage.getInstance().
                getBooleanPreferences(SharedPreferenceStorage.SettingKeys.SCREEN_ON, true));

        screenOnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                sharedPreferenceStorage.addSettingsPreferences(SharedPreferenceStorage.SettingKeys.SCREEN_ON, isChecked);
            }
        });

        soundSwitch.setChecked(SharedPreferenceStorage.getInstance().
                getBooleanPreferences(SharedPreferenceStorage.SettingKeys.SOUNDS, true));

        soundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                sharedPreferenceStorage.addSettingsPreferences(SharedPreferenceStorage.SettingKeys.SOUNDS, isChecked);
            }
        });

        resetCalibration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder adb=new AlertDialog.Builder(SettingsActivity.this);
                adb.setTitle(getApplicationContext().getResources().getString(R.string.reset_calibration));
                adb.setMessage(getApplicationContext().getResources().getString(R.string.reset_calibration_description));
                adb.setNegativeButton(getApplicationContext().getResources().getString(R.string.no), null);
                adb.setPositiveButton(getApplicationContext().getResources().getString(R.string.yes), new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        AccelerometerHandler.getInstance().resetCalibration();
                    }});
                adb.show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case TransparentBackground.CAMERA_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    TransparentBackground.setPermission(true);
                }
                else{
                    TransparentBackground.setPermission(false);
                    backgroundSwitch.setChecked(false);
                }
            }
        }
    }
}
