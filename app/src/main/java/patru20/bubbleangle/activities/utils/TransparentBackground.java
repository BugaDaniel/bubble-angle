package patru20.bubbleangle.activities.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.view.SurfaceView;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

import patru20.bubbleangle.R;
import patru20.bubbleangle.Utils;
import patru20.bubbleangle.orientation.OrientationChangeListener;
import patru20.bubbleangle.orientation.OrientationType;
import patru20.bubbleangle.storage.SharedPreferenceStorage;

public class TransparentBackground implements OrientationChangeListener {

    private Camera camera;
    private SurfaceView surfaceView;
    private OrientationType orientationType;
    public static final int CAMERA_PERMISSION = 77;
    private boolean isCameraEnabled;
    private FrameLayout frameLayout;

    public TransparentBackground(SurfaceView surfaceView, FrameLayout fragmentHolder){

        this.frameLayout = fragmentHolder;
        this.surfaceView = surfaceView;

        ViewTreeObserver viewTreeObserver = surfaceView.getViewTreeObserver();

        viewTreeObserver.addOnWindowFocusChangeListener(new ViewTreeObserver.OnWindowFocusChangeListener() {
            @Override
            public void onWindowFocusChanged(boolean hasFocus) {
                setCameraPreviewFromOrientation();

                if(frameLayout != null){
                    if(!isCameraEnabled){
                        frameLayout.setBackgroundColor(frameLayout.getContext().getResources().getColor(R.color.dark_gray));
                    }
                    else{
                        frameLayout.setBackgroundColor(Color.TRANSPARENT);
                    }
                }
            }
        });
    }

    /** Check if this device has a camera */
    private static boolean checkCameraHardware(Context context) {

        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public static void askPermission(Activity activity){

        if(checkCameraHardware(activity.getApplicationContext())){

            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA},
                    CAMERA_PERMISSION);
        }
    }

    private void setDisplayOrientation(){
        if(isCameraEnabled && camera != null && orientationType != null){
            try {
                camera.setDisplayOrientation(getCameraDisplayOrientationTiltDegrees());

            }catch (RuntimeException e){

                if(frameLayout != null){
                    frameLayout.setBackgroundColor(frameLayout.getContext().getResources().getColor(R.color.dark_gray));
                    Context context = frameLayout.getContext();
                    Utils.showToast(context, context.getResources().getString(R.string.camera_did_not_load));
                }
            }
        }
    }

    private int getCameraDisplayOrientationTiltDegrees() {

        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);

        int degrees = 0;

        switch (orientationType) {

            case PORTRAIT_UP:
                degrees = 0;
                break;
            case LANDSCAPE_LEFT:
                degrees = 90;
                break;
            case PORTRAIT_DOWN:
                degrees = 180;
                break;
            case LANDSCAPE_RIGHT:
                degrees = 270;
                break;
        }
        return (info.orientation - degrees + 360) % 360;
    }

    private void setCameraPreviewFromOrientation(){
            if(shouldOpenCamera()){
                startCamera();
            }
            else{
                stopCamera();
            }
    }

    public void start(){

        if(isCameraEnabled){
            try {
                if(camera == null){
                    camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                    setCameraPreviewFromOrientation();
                }

            } catch (Exception e) {
                Log.e("tag", "init_camera: " + e);
            }
        }
    }

    public void stop(){

        if(isCameraEnabled){
            stopCamera();
        }
    }

    private void stopPreview(){
        // stop preview before making changes
        if(camera != null){
            try {
                camera.stopPreview();
                camera.release();
                camera = null;
            } catch (Exception e) {
                // ignore: tried to stop a non-existent preview
            }
        }
    }

    private void startPreview(){

        if(camera != null){
            // start preview with new settings
            try {
                camera.setPreviewDisplay(surfaceView.getHolder());
                camera.startPreview();
                camera.autoFocus(null);
            } catch (Exception e) {
                Log.d("TAG", "Error starting camera preview: " + e.getMessage());
            }
        }
    }

    public void pauseCamera(){
        stopPreview();
    }

    public void resumeCamera(){
        start();
        setDisplayOrientation();
        startPreview();
    }

    private void startCamera(){
        resumeCamera();
        surfaceView.setBackgroundColor(Color.TRANSPARENT);
    }

    private void stopCamera(){
        pauseCamera();
        surfaceView.setBackgroundColor(surfaceView.getContext().getResources().getColor(R.color.dark_gray));
    }

    @Override
    public void onOrientationChanged(OrientationType orientationType) {
        this.orientationType = orientationType;
        if(isCameraEnabled){
            setCameraPreviewFromOrientation();
        }
    }

    private boolean shouldOpenCamera(){
        return !(orientationType == OrientationType.FACE_UP || orientationType == OrientationType.FACE_DOWN);
    }

    public boolean isBackgroundEnabled() {

        isCameraEnabled = SharedPreferenceStorage.getInstance()
                .getBooleanPreferences(SharedPreferenceStorage.SettingKeys.BACKGROUND, false);

        return isCameraEnabled;
    }

    private static boolean permissionGranted = SharedPreferenceStorage.getInstance().getBooleanPreferences(SharedPreferenceStorage.SettingKeys.PERMISSION_GRANTED, false);

    public static void setPermission(boolean isPermissionGranted){
        permissionGranted = isPermissionGranted;
        SharedPreferenceStorage.getInstance().addSettingsPreferences(SharedPreferenceStorage.SettingKeys.PERMISSION_GRANTED, isPermissionGranted);
    }

    public boolean getPermission(){
        return permissionGranted;
    }
}
