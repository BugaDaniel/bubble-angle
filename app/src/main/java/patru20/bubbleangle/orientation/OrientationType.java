package patru20.bubbleangle.orientation;

public enum OrientationType {

    PORTRAIT_UP(0),
    PORTRAIT_DOWN(1),
    LANDSCAPE_LEFT(2),
    LANDSCAPE_RIGHT(3),
    FACE_UP(4),
    FACE_DOWN(5);

    private int orientation;

    OrientationType(int orientation){
        this.orientation = orientation;
    }

    public int getValue() {
        return orientation;
    }
}
