package patru20.bubbleangle.orientation;

public interface OrientationChangeListener {

    void onOrientationChanged(OrientationType orientation);

}
