package patru20.bubbleangle.orientation;

import androidx.annotation.NonNull;
import android.view.Display;
import android.view.Surface;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class OrientationManager{

    private List<WeakReference<OrientationChangeListener>> orientationChangeListeners = new ArrayList<>();
    private OrientationType orientation;
    private OrientationType lastOrientation;
    private Display display;

    private static OrientationManager orientationManagerInstance;

    public static synchronized OrientationManager getInstance(){
        if(orientationManagerInstance == null){
            orientationManagerInstance = new OrientationManager();
        }
        return orientationManagerInstance;
    }

    public void initialize(Display display){
        this.display = display;
    }

    public OrientationType calculateOrientation(float[] accelerometerValues){

        int inclination = getInclination(accelerometerValues);

        if(orientation != null && orientation != OrientationType.FACE_UP && orientation != OrientationType.FACE_DOWN){
            if(inclination < 25 || inclination > 155){
                if(inclination < 25){
                    orientation = OrientationType.FACE_UP;
                }
                else {
                    orientation = OrientationType.FACE_DOWN;
                }
            }
            else{
                handleRotation();
            }
        }
        else{
            if(inclination < 55 || inclination > 125){

                if(inclination < 55){
                    orientation = OrientationType.FACE_UP;
                }
                else {
                    orientation = OrientationType.FACE_DOWN;
                }
            }
            else{
                handleRotation();
            }
        }

        if(!orientation.equals(lastOrientation)){

            for (WeakReference<OrientationChangeListener> weakReference : orientationChangeListeners) {
                OrientationChangeListener current = weakReference.get();
                if(current != null){
                    current.onOrientationChanged(orientation);
                }
            }
            lastOrientation = orientation;
        }
        return orientation;
    }

    private int getInclination(float[] accelerometerValues){
        return  (int) Math.round(Math.toDegrees(Math.acos(accelerometerValues[2])));
    }

    public void registerOrientationChangeListener(@NonNull OrientationChangeListener callback){
        orientationChangeListeners.add(new WeakReference<>(callback));
    }

    private void handleRotation(){

        switch (display.getRotation()) {

            case Surface.ROTATION_0:
                orientation = OrientationType.PORTRAIT_UP;
                break;
            case Surface.ROTATION_90:
                orientation = OrientationType.LANDSCAPE_LEFT;
                break;
            case Surface.ROTATION_180:
                orientation = OrientationType.PORTRAIT_DOWN;
                break;
            case Surface.ROTATION_270:
                orientation = OrientationType.LANDSCAPE_RIGHT;
                break;
        }
    }

    public OrientationType getOrientation(){
        return orientation;
    }
}
