package patru20.bubbleangle.fragments.core;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import patru20.bubbleangle.R;
import patru20.bubbleangle.angle.AccelerometerHandler;
import patru20.bubbleangle.fragments.FlatFragment;
import patru20.bubbleangle.fragments.LandscapeFragment;
import patru20.bubbleangle.fragments.PortraitFragment;
import patru20.bubbleangle.orientation.OrientationChangeListener;
import patru20.bubbleangle.orientation.OrientationType;

public class OrientationFragmentManager implements OrientationChangeListener {

    private FragmentManager fragmentManager;

    public OrientationFragmentManager(FragmentManager fragmentManager){
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void onOrientationChanged(OrientationType orientation) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        BaseOrientationFragment fragment;

        if(orientation.equals(OrientationType.LANDSCAPE_RIGHT) || orientation.equals(OrientationType.LANDSCAPE_LEFT)){
            fragment = new LandscapeFragment();
        }
        else if(orientation.equals(OrientationType.PORTRAIT_DOWN) || orientation.equals(OrientationType.PORTRAIT_UP)){
            fragment = new PortraitFragment();
        }
        else {
            fragment = new FlatFragment();
        }
        AccelerometerHandler.getInstance().registerAngleChangeListener(fragment);
        AccelerometerHandler.getInstance().registerPercentageChangeListener(fragment);
        fragmentTransaction.replace(R.id.fragment_placeholder, fragment);

        // fragmentTransaction.commit(); java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
        fragmentTransaction.commitAllowingStateLoss();
    }
}
