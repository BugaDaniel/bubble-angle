package patru20.bubbleangle.fragments.core;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import androidx.fragment.app.Fragment;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import patru20.bubbleangle.R;
import patru20.bubbleangle.activities.MainActivity;
import patru20.bubbleangle.angle.AccelerometerHandler;
import patru20.bubbleangle.angle.AngleChangeListener;
import patru20.bubbleangle.angle.PercentageChangeListener;
import patru20.bubbleangle.storage.SharedPreferenceStorage;

public abstract class BaseOrientationFragment extends Fragment implements AngleChangeListener, PercentageChangeListener {

    protected ImageView bubble;
    protected TextView angleTextX;
    protected TextView angleTextY;
    protected TextView angleTextXOver;
    protected TextView angleTextYOver;
    protected int maxAngle = 50;

    @Override
    public void onAngleChange(double angle) { }

    @Override
    public void onAngleChange(double angleX, double angleY) { }

    @Override
    public final void onPercentageChanged(float percentage) {
        if(angleTextXOver != null){
            angleTextXOver.setAlpha(percentage);
            onPercentageChangedInternal(percentage);
        }
    }

    public void onPercentageChangedInternal(float percentage){}

    protected String formatAngleDecimals(double unformattedAngle, String angleDirection){

        String format = "%.2f";

        if(!AccelerometerHandler.getInstance().isGravitySensorPresent()){
            format = "%.1f";
        }

        String result = String.format(Locale.ROOT, format, unformattedAngle);

        return (unformattedAngle < 0 ) ? (angleDirection + ": " + result + "\u00B0")
                : (angleDirection + ":  " + result + "\u00B0");
    }

    protected void setToolbarBackground(boolean setTransparent){
        MainActivity mainActivity = (MainActivity) getActivity();
        if(mainActivity != null)
            mainActivity.setToolbarBackground(setTransparent);
    }

    private boolean madeSound = false;
    private int lastColor;

    protected void changeBubbleColor(double... angles){

        int color = Color.WHITE;
        boolean changeBubbleColorToGreen = true;

        for (double angle: angles) {
            if((Math.abs(angle)) > 1){
                changeBubbleColorToGreen = false;
            }
        }

        if(changeBubbleColorToGreen){

            color = Color.GREEN;

            if(SharedPreferenceStorage.getInstance().
                    getBooleanPreferences(SharedPreferenceStorage.SettingKeys.SOUNDS, true)){

                if(!madeSound){
                    makeSound();
                    madeSound = true;
                }
            }

        }
        else {
            madeSound = false;
        }

        if(color != lastColor){
            bubble.getDrawable().setColorFilter(color, PorterDuff.Mode.MULTIPLY );
            lastColor = color;
        }
    }

    MediaPlayer mediaPlayer;

    private void makeSound(){
        mediaPlayer.start();
    }

    @Override
    public void onStart() {
        super.onStart();
        mediaPlayer = MediaPlayer.create(getContext(), R.raw.ding);
    }

    @Override
    public void onStop() {
        super.onStop();
        mediaPlayer.release();
    }
}
