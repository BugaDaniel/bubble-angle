package patru20.bubbleangle.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import patru20.bubbleangle.Utils;
import patru20.bubbleangle.fragments.core.BaseOrientationFragment;
import patru20.bubbleangle.R;

public class FlatFragment extends BaseOrientationFragment {

    private FrameLayout circleBase;
    private FrameLayout frameLayout;
    private LinearLayout linearLayout;
    private LinearLayout linearLayoutOver;
    private FrameLayout frameLayoutOverlay;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.flat_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        angleTextX = view.findViewById(R.id.angle_x_flat);
        angleTextY = view.findViewById(R.id.angle_y_flat);
        bubble = view.findViewById(R.id.circle_flat);
        circleBase = view.findViewById(R.id.circle_base);
        frameLayout = view.findViewById(R.id.layout_flat);
        linearLayout = view.findViewById(R.id.linearLayout_with_text_views);
        linearLayoutOver = view.findViewById(R.id.linearLayout_with_text_views_overlay);
        angleTextXOver = view.findViewById(R.id.angle_x_flat_overlay);
        angleTextYOver = view.findViewById(R.id.angle_y_flat_overlay);
        frameLayoutOverlay = view.findViewById(R.id.frame_overlay);
      }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAngleChange(double angleX, double angleY) {

        if(angleTextX == null || angleTextY == null){
            return;
        }


        if(frameLayout != null){

            Display display = frameLayout.getDisplay();

            if(display != null){

                int rotation = display.getRotation();

                if(rotationChanged(rotation)){
                    changeDegreeViews(rotation);
                }

                switch (rotation){

                    case Surface.ROTATION_90:
                        handleFlatOrientationChange(angleY, -angleX);
                        break;
                    case Surface.ROTATION_270:
                        handleFlatOrientationChange(-angleY, angleX);
                        break;
                    case Surface.ROTATION_180:
                        handleFlatOrientationChange(-angleX, -angleY);
                        break;
                    case Surface.ROTATION_0:
                        handleFlatOrientationChange(angleX, angleY);
                }
            }
        }

        changeBubbleColor(angleX,angleY);
    }

    private void handleFlatOrientationChange(double angleX, double angleY){
        angleTextX.setText(formatAngleDecimals(angleX, "X"));
        angleTextY.setText(formatAngleDecimals(angleY, "Y"));
        angleTextXOver.setText(formatAngleDecimals(angleX, "X"));
        angleTextYOver.setText(formatAngleDecimals(angleY, "Y"));
        moveBubble((float) angleX, (float)angleY);
    }

    private void moveBubble(float angleIntX, float angleIntY){
        int maxAngle = 55;

        int maximMovement = circleBase.getHeight()/2 - bubble.getHeight()/2 - circleBase.getHeight()/ 60  ;
        int startingPoint = circleBase.getHeight()/2 - bubble.getHeight()/2;
        int unitToMove = startingPoint / maxAngle;

        float rv;

        rv = (float)Math.sqrt(Math.pow(angleIntX, 2) + Math.pow(angleIntY, 2)) * unitToMove;

        if(rv > maximMovement){
            angleIntX =(angleIntX * maximMovement/rv);
            angleIntY =(angleIntY * maximMovement/rv);
        }

        float howMuchToMoveX = startingPoint + unitToMove * angleIntX;
        float howMuchToMoveY = startingPoint + unitToMove * angleIntY;
        bubble.setX(howMuchToMoveX);
        bubble.setY(howMuchToMoveY);
    }

    private FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.WRAP_CONTENT,
            FrameLayout.LayoutParams.WRAP_CONTENT);

    private void changeDegreeViews(int rotation){

        int gravity;
        int bottomMargin;
        int marginStart;
        int linearLayoutOrientation;
        boolean setTransparent;

        //landscape
        if(rotation != Surface.ROTATION_180 &&
                rotation != Surface.ROTATION_0){

            gravity = Gravity.CENTER_VERTICAL;
            bottomMargin = 0;
            linearLayoutOrientation = LinearLayout.VERTICAL;
            setTransparent = true;
            marginStart = 30;
        }
        // portrait
        else{

            gravity = Gravity.BOTTOM|Gravity.CENTER;
            bottomMargin = 60;
            linearLayoutOrientation = LinearLayout.HORIZONTAL;
            setTransparent = false;
            marginStart = 0;
        }

        params.gravity = gravity;
        params.bottomMargin = Utils.convertDpiToPx(getContext(), bottomMargin);
        params.setMarginStart(Utils.convertDpiToPx(getContext(), marginStart));
        frameLayoutOverlay.setLayoutParams(params);
        linearLayout.setOrientation(linearLayoutOrientation);
        linearLayoutOver.setOrientation(linearLayoutOrientation);
        setToolbarBackground(setTransparent);
    }

    private int lastRotation = -77;

    private boolean rotationChanged(int rotation){
        if(lastRotation != -77 ){
            if(isEven(lastRotation)){
                if(isEven(rotation)){
                    return false;
                }
            }
            else{
                if(!isEven(rotation)){
                    return false;
                }
            }
        }

        lastRotation = rotation;
        return true;
    }

    private boolean isEven(int number){
        if(number % 2 == 0){
            return true;
        }
        return false;
    }

    @Override
    public void onPercentageChangedInternal(float percentage) {
        linearLayoutOver.setAlpha(percentage);
    }
}
