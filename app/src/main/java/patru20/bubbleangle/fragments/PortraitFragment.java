package patru20.bubbleangle.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

import patru20.bubbleangle.fragments.core.BaseOrientationFragment;
import patru20.bubbleangle.R;

public class PortraitFragment extends BaseOrientationFragment {

    private GridLayout gridLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.portrait_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        angleTextX = view.findViewById(R.id.angle_portrait);
        bubble = view.findViewById(R.id.bubble_portrait);
        gridLayout = view.findViewById(R.id.grid_portrait);
        angleTextXOver = view.findViewById(R.id.angle_portrait_overlay);
        setToolbarBackground(false);
    }

    @Override
    public void onAngleChange(double angle) {
        if(angleTextX == null){
            return;
        }
        String formatAngleDecimals = formatAngleDecimals(angle, "X");
        angleTextX.setText(formatAngleDecimals);
        moveBubble((float) angle);
        changeBubbleColor(angle);
        angleTextXOver.setText(formatAngleDecimals);
    }

    private void moveBubble(float angle){

        int width = gridLayout.getWidth();
        int startingPoint = width / 2 - bubble.getWidth() / 2;
        int unitToMove = startingPoint / (maxAngle + 5);

        if(angle > maxAngle){
            angle = maxAngle;
        }
        else if (angle < -maxAngle){
            angle = -maxAngle;
        }

        bubble.setX(startingPoint + unitToMove * angle);
    }
}
