package patru20.bubbleangle.angle;

import android.app.Application;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import patru20.bubbleangle.angle.filters.core.Reseter;
import patru20.bubbleangle.orientation.OrientationManager;
import patru20.bubbleangle.angle.filters.core.FilterManager;
import patru20.bubbleangle.orientation.OrientationType;
import patru20.bubbleangle.storage.SharedPreferenceStorage;

import static android.content.Context.SENSOR_SERVICE;

public class AccelerometerHandler{

    private float[] accelerometerValuesNormalized;
    private double lastAngleX;
    private double lastAngleY;

    private AngleChangeListener angleChangeListener;

    private SensorManager sensorManager;
    private Sensor sensor;

    private FilterManager filterManager;
    private boolean isGravitySensorPresent = true;
    private Reseter reseter;
    private StableAngleChecker stableAngleChecker;
    private int delay = 2000;

    private PercentageChangeListener percentageChangeListener;
    private float lastPercentage = 0;
    private CalibrationHandler calibrationHandler;

    private AccelerometerHandler(){

        reseter = new Reseter(new Reseter.Callback() {
            @Override
            public void reset() {
                if(filterManager != null) {
                    filterManager.reset();
                }
                stableAngleChecker.reset();
            }
        });
    }

    private static AccelerometerHandler accelerometerHandlerInstance;

    public static synchronized AccelerometerHandler getInstance(){
        if(accelerometerHandlerInstance == null){
            accelerometerHandlerInstance = new AccelerometerHandler();
        }
        return accelerometerHandlerInstance;
    }

    public void initialize(Application context){

        this.sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);

        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        if(sensor == null){
            isGravitySensorPresent = false;
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }

        stableAngleChecker = new StableAngleChecker();
        calibrationHandler = new CalibrationHandler(context);
    }

    private void calculateAngle(OrientationType orientation) {

        if(filterManager == null){
            filterManager = new FilterManager(isGravitySensorPresent, delay);
        }

        double xDegree = 0;
        double yDegree = 0;
        double[] filteredAngles;

        switch (orientation) {
            case PORTRAIT_UP:
                xDegree = filterManager.getFilteredAngle(Math.atan2(accelerometerValuesNormalized[0], accelerometerValuesNormalized[1]));
                xDegree = calibrationHandler.getCalibratedAngle(SharedPreferenceStorage.CalibrationKeys.PORTRAIT_UP_CALIBRATED, xDegree);
                break;
            case PORTRAIT_DOWN:
                xDegree = filterManager.getFilteredAngle(Math.atan2(-accelerometerValuesNormalized[0], -accelerometerValuesNormalized[1]));
                xDegree = calibrationHandler.getCalibratedAngle(SharedPreferenceStorage.CalibrationKeys.PORTRAIT_DOWN_CALIBRATED, xDegree);
                break;
            case LANDSCAPE_RIGHT:
                xDegree =  filterManager.getFilteredAngle(Math.atan2(accelerometerValuesNormalized[1], -accelerometerValuesNormalized[0]));
                xDegree = calibrationHandler.getCalibratedAngle(SharedPreferenceStorage.CalibrationKeys.LANDSCAPE_RIGHT_CALIBRATED, xDegree);

                break;
            case LANDSCAPE_LEFT:
                xDegree =  filterManager.getFilteredAngle(Math.atan2(-accelerometerValuesNormalized[1], accelerometerValuesNormalized[0]));
                xDegree = calibrationHandler.getCalibratedAngle(SharedPreferenceStorage.CalibrationKeys.LANDSCAPE_LEFT_CALIBRATED, xDegree);
                break;
            case FACE_UP:
                filteredAngles =filterManager.getFilteredAngles(Math.atan2(accelerometerValuesNormalized[0], accelerometerValuesNormalized[2]),
                        Math.atan2(-accelerometerValuesNormalized[1], accelerometerValuesNormalized[2]));

                xDegree = filteredAngles[0];
                yDegree = filteredAngles[1];

                xDegree = calibrationHandler.getCalibratedAngle(SharedPreferenceStorage.CalibrationKeys.FACE_UP_CALIBRATED_X, xDegree);
                yDegree = calibrationHandler.getCalibratedAngle(SharedPreferenceStorage.CalibrationKeys.FACE_UP_CALIBRATED_Y, yDegree);
                break;
            case FACE_DOWN:
                filteredAngles = filterManager.getFilteredAngles(Math.atan2(accelerometerValuesNormalized[0], -accelerometerValuesNormalized[2]),
                        Math.atan2(-accelerometerValuesNormalized[1], -accelerometerValuesNormalized[2]));

                xDegree = filteredAngles[0];
                yDegree = filteredAngles[1];

                xDegree = calibrationHandler.getCalibratedAngle(SharedPreferenceStorage.CalibrationKeys.FACE_DOWN_CALIBRATED_X, xDegree);
                yDegree = calibrationHandler.getCalibratedAngle(SharedPreferenceStorage.CalibrationKeys.FACE_DOWN_CALIBRATED_Y, yDegree);
        }

        if (orientation.getValue() > 3) {

            if (xDegree != lastAngleX || yDegree != lastAngleY && angleChangeListener != null) {
                angleChangeListener.onAngleChange(xDegree, yDegree);
                lastAngleX = xDegree;
                lastAngleY = yDegree;
            }

        } else if (xDegree != lastAngleX && angleChangeListener != null) {
            angleChangeListener.onAngleChange(xDegree);
            lastAngleX = xDegree;
        }

        float percentage = stableAngleChecker.getCalculatedPercentageForAlpha(xDegree);

        if(percentage == 1){
            calibrationHandler.calibrate(new float[]{(float) xDegree, (float) yDegree}, percentage);
        }

        if(percentage != lastPercentage){
            lastPercentage = percentage;
            percentageChangeListener.onPercentageChanged(percentage);
        }
    }

    public void registerAngleChangeListener(AngleChangeListener angleChangeListener){
        this.angleChangeListener = angleChangeListener;
    }

    public void stopAccelerometer(){
        sensorManager.unregisterListener(sensorEventListener, sensor);

        if(isGravitySensorPresent){
            sensorManager.unregisterListener(sensorEventListener,sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
        }
    }

    public void startAccelerometer(){

        int sensorDelay;

        if(isGravitySensorPresent){
            sensorDelay = SensorManager.SENSOR_DELAY_FASTEST;
        }
        else{
            sensorDelay = SensorManager.SENSOR_DELAY_UI;
        }

        sensorManager.registerListener(sensorEventListener, sensor, sensorDelay);

        if(isGravitySensorPresent){
            sensorManager.registerListener(sensorEventListener,sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) , 60);
        }
     }

    private SensorEventListener sensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {

            // if gravity present - if is true, accel reaches only reseter
            // if no gravity - if is true, accel reaches if + reseter
            if(sensor.getType() == event.sensor.getType()) {
                accelerometerValuesNormalized = normalizeAccelerometer(event);
                calculateAngle(OrientationManager.getInstance().calculateOrientation(accelerometerValuesNormalized));
            }

            reseter.check(event);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {}
    };

    private float[] normalizeAccelerometer(SensorEvent event){
        float[] g;
        g = event.values.clone();
        double norm_Of_g = Math.sqrt(g[0] * g[0] + g[1] * g[1] + g[2] * g[2]);

        // Normalize the accelerometer vector
        g[0] = g[0] / (float) norm_Of_g;
        g[1] = g[1] / (float) norm_Of_g;
        g[2] = g[2] / (float) norm_Of_g;

        return g;
    }

    public boolean isGravitySensorPresent(){
        return isGravitySensorPresent;
    }

    public void registerPercentageChangeListener(PercentageChangeListener percentageChangeListener){
        this.percentageChangeListener = percentageChangeListener;
    }

    public void startCalibration(){
        if(calibrationHandler != null){
            calibrationHandler.start();
        }
    }

    public void resetCalibration(){
        calibrationHandler.reset();
    }

    public boolean isCalibrationStarted(){
        return calibrationHandler.isCalibrationStarted();
    }

    public void registerCalibrationHandler(CalibrationHandler.CalibrationCompleteCallback calibrationCompleteCallback){
        calibrationHandler.registerCalibrationCompleteCallback(calibrationCompleteCallback);
    }

    public void cancelCalibration(){
        calibrationHandler.cancelCalibration();
    }
}
