package patru20.bubbleangle.angle;

public interface PercentageChangeListener {

    void onPercentageChanged(float percentage);
}
