package patru20.bubbleangle.angle;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import patru20.bubbleangle.R;
import patru20.bubbleangle.Utils;
import patru20.bubbleangle.orientation.OrientationManager;
import patru20.bubbleangle.orientation.OrientationType;
import patru20.bubbleangle.storage.SharedPreferenceStorage;

public class CalibrationHandler{

    private float resultX;
    private float resultY;
    private SharedPreferenceStorage sharedPreferenceStorage = SharedPreferenceStorage.getInstance();
    private boolean calibrationStarted = false;
    private Context context;
    private Handler handler;
    private float percentage;

    public CalibrationHandler(Context context){
        this.context = context;
        handler = new Handler(Looper.getMainLooper());
    }

    public void calibrate(float[] filteredAngle, float percentage){

        this.percentage = percentage;

        if(calibrationStarted){
            OrientationType orientationType = OrientationManager.getInstance().getOrientation();

            if(orientationType != OrientationType.FACE_UP && orientationType != OrientationType.FACE_DOWN){

                resultX = -filteredAngle[0];
                sharedPreferenceStorage.addCalibrationPreferences(new float[]{resultX});

            }
            else{

                resultX = -filteredAngle[0];
                resultY = -filteredAngle[1];
                sharedPreferenceStorage.addCalibrationPreferences(new float[]{resultX, resultY});
            }

            Utils.showToast(context,context.getResources().getString(R.string.calibration_complete));

            stopCalibration();
        }
    }

    public void reset(){
        sharedPreferenceStorage.resetSharedPreferencesCalibration();
    }

    public void start(){

        Utils.showToast(context, context.getResources().getString(R.string.keep_phone_steady_flat_surface));
        calibrationStarted = true;

        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(percentage != 1){
                    Utils.showToast(context, context.getResources().getString(R.string.keep_phone_steady_flat_surface));
                }
                handler.postDelayed(this,5000);
            }
        },5000);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                cancelCalibration();
            }
        },20000);
    }

    public boolean isCalibrationStarted(){
        return calibrationStarted;
    }

    public double getCalibratedAngle(SharedPreferenceStorage.CalibrationKeys calibrationKeys, double angles){
        if(!calibrationStarted){
            return angles + sharedPreferenceStorage.getDoublePreferences(calibrationKeys,0);
        }
        return angles;
    }

    private void stopCalibration(){
        handler.removeCallbacksAndMessages(null);
        calibrationStarted = false;

        if(calibrationCompleteCallback != null){
            calibrationCompleteCallback.calibrationComplete();
        }
    }

    private CalibrationCompleteCallback calibrationCompleteCallback;

    public void registerCalibrationCompleteCallback(CalibrationCompleteCallback calibrationCompleteCallback){
        this.calibrationCompleteCallback = calibrationCompleteCallback;
    }

    public interface CalibrationCompleteCallback {
        void calibrationComplete();
    }

    public void cancelCalibration(){
        Utils.showToast(context, context.getResources().getString(R.string.calibration_canceled));
        stopCalibration();
    }
}
