package patru20.bubbleangle.angle.filters;

import patru20.bubbleangle.angle.filters.core.IFilter;

public class BoxCarLPF implements IFilter {

    private double output = -777;
    private double alpha = 0.5d;

    @Override
    public double getFilteredAccelerometerData(double accelerometerData) {
        return boxCar(accelerometerData);
    }

    private double boxCar(double accelerometerData) {

        if(output == -777){
            output = accelerometerData;
        }

        // y[i] := y[i-1] + α * (x[i] - y[i-1])
        output = output + alpha * (accelerometerData - output);

        return output;
    }

    @Override
    public void reset() { }
}
