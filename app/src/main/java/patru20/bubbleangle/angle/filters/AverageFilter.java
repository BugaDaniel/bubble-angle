package patru20.bubbleangle.angle.filters;

import patru20.bubbleangle.angle.filters.core.IFilter;

public class AverageFilter implements IFilter {

    private int numberOfValues;
    private double[] values;
    private int index = 0;

    private boolean firstTime = true;

    public AverageFilter(int numberOfValues){
        this.numberOfValues = numberOfValues;
        values = new double[numberOfValues];
    }

    @Override
    public double getFilteredAccelerometerData(double accelerometerData) {

        if(firstTime){
            firstTime = false;
            for (int i=0; i<numberOfValues; i++){
                values[i] = accelerometerData;
            }
        }

        if(index < numberOfValues){
            values[index] = accelerometerData;
        }else {
            index = 0;
            values[index] = accelerometerData;

        }
        index++;

        return averageValues();
    }

    @Override
    public void reset() {
        firstTime = true;
        values = new double[numberOfValues];
        index = 0;
    }


    private double averageValues(){
        double sum = 0;

        for (double value: values) {
            sum += value;
        }
        return sum / numberOfValues;
    }
}
