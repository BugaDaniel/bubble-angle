package patru20.bubbleangle.angle.filters.core;

public interface IFilter {

    double getFilteredAccelerometerData(double accelerometerData);

    void reset();
}
