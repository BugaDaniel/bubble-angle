package patru20.bubbleangle.angle.filters.core;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.util.Log;

public class Reseter {

    private Callback callback;
    private double lastValue;
    private String TAG = "moving";

    public Reseter(Callback callback){
        this.callback = callback;
    }

    private boolean isMoving(float[] values, float tolerance){
        double currentValue = Math.sqrt(Math.pow(values[0],2) + Math.pow(values[1],2) + Math.pow(values[2],2));
        Log.v(TAG, "diff = " + Math.abs(currentValue - lastValue) + " tolerance = " + tolerance);
        if(Math.abs(currentValue - lastValue) > tolerance){
            lastValue = currentValue;
            return true;
        }
        lastValue = currentValue;

        return false;
    }

    public void check(SensorEvent event){
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if (isMoving(event.values, 0.17f)) {
                Log.v(TAG, "isMoving");
                callback.reset();
            }
        }
    }

    public interface Callback{
        void reset();
    }
}
