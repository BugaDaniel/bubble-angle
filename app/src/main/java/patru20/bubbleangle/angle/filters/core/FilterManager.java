package patru20.bubbleangle.angle.filters.core;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.List;

import patru20.bubbleangle.angle.filters.AverageFilter;
import patru20.bubbleangle.angle.filters.BoxCarLPF;
import patru20.bubbleangle.angle.filters.ButterworthLPF;

public class FilterManager {

    private List<IFilter> butterworthList;
    private List<IFilter> boxCarList;
    private List<IFilter> averageList;
    private int numberOfFilters = 2;
    private boolean isGravitySensorPresent;
    private Handler handler = new Handler(Looper.getMainLooper());
    private boolean aggressive = false;
    private int delay;

    public FilterManager(boolean isGravitySensorPresent, int delay){

        this.delay = delay;
        this.isGravitySensorPresent = isGravitySensorPresent;

        butterworthList = new ArrayList<>();
        boxCarList = new ArrayList<>();
        averageList = new ArrayList<>();

        int numberOfValues;

        if(isGravitySensorPresent){
            numberOfValues = 300;
        }
        else{
            numberOfValues = 600;
        }

        for(int i=0; i<numberOfFilters; i++){
            butterworthList.add( new ButterworthLPF());
            boxCarList.add(new BoxCarLPF());
            averageList.add(new AverageFilter(numberOfValues));
        }

        startAggressiveTimer();
    }

    public double getFilteredAngle(double accelerometerData){
        return getAngleFromFilteredAccelerometer(getFilteredAccelerometerData(0, accelerometerData));
    }

    public double[] getFilteredAngles(double accelerometerDataX, double accelerometerDataY){

        double[] outputValues = new double[numberOfFilters];

        outputValues[0] = accelerometerDataX;
        outputValues[1] = accelerometerDataY;

        for(int i=0; i<numberOfFilters; i++) {
            outputValues[i] = getAngleFromFilteredAccelerometer(getFilteredAccelerometerData(i,outputValues[i]));
        }

        return outputValues;
    }

    private double getFilteredAccelerometerData(int index, double accelerometerData){

        double currentAccelerometerData = accelerometerData;

        currentAccelerometerData = getFilteredAccelerometerDataInternal(butterworthList.get(index), currentAccelerometerData);
        currentAccelerometerData = getFilteredAccelerometerDataInternal(boxCarList.get(index), currentAccelerometerData);

        if(aggressive) {
            currentAccelerometerData = getFilteredAccelerometerDataInternal(averageList.get(index), currentAccelerometerData);
        }

        return currentAccelerometerData;
    }

    private double getFilteredAccelerometerDataInternal(IFilter filter, double accelerometerData){
        return filter.getFilteredAccelerometerData(accelerometerData);
    }

    private double getAngleFromFilteredAccelerometer(double filteredAccelerometerData){
        return removeUnwantedDecimals(Math.toDegrees(filteredAccelerometerData));
    }

    private double removeUnwantedDecimals(double value){

        int decimalMultiplier = 100;

        if(!isGravitySensorPresent){
            decimalMultiplier = 10;
        }

        double output = value * decimalMultiplier;
        output = Math.round(output);
        output = output / decimalMultiplier;

        return output;
    }

    private void startAggressiveTimer(){
        aggressive = false;
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                aggressive = true;
            }
        },delay);
    }

    public void reset(){
        for (IFilter filter: averageList) {
            filter.reset();
        }

        startAggressiveTimer();
    }
}
