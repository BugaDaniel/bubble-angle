package patru20.bubbleangle.angle.filters;

import patru20.bubbleangle.angle.filters.core.IFilter;
import uk.me.berndporr.iirj.Butterworth;

public class ButterworthLPF implements IFilter {

    private Butterworth butterworth;

    public ButterworthLPF(){
        butterworth = new Butterworth();
        butterworth.lowPass(2, 60, 4);
    }

    @Override
    public double getFilteredAccelerometerData(double accelerometerData) {
        return butterworth.filter(accelerometerData);
    }

    @Override
    public void reset() {
        butterworth.reset();
    }
}
