package patru20.bubbleangle.angle;

public interface AngleChangeListener {

    void onAngleChange(double angle);

    void onAngleChange(double angleX,double angleY);
}
