package patru20.bubbleangle.angle;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class StableAngleChecker {

    private Handler handler;

    private double lastAngle;
    private double currentAngle;

    private int calculatedPercentage;
    private double maximumDifference;

    private String TAG = "percentage";

    public StableAngleChecker(){
        handler = new Handler(Looper.getMainLooper());
        reset();
    }

    private int counter;

    private int differenceToPercentage(double angle){

        if(lastAngle == 0){
            return 0;
        }

        if(maximumDifference == 0){
            maximumDifference = Math.abs(lastAngle -angle);
        }

        double currentDif = Math.abs(lastAngle - angle);

        int currentPercentage = 0;

        if(currentDif == 0){
            counter++;

            if(counter > 50){
                counter = 50;
            }
            currentPercentage = 2 * counter;

        }else {
            counter = 0;
        }

        return currentPercentage;
    }

    private int calcDelay = 70;
    private int currentCalculatedPercentage;

    public void reset(){

        handler.removeCallbacksAndMessages(null);

        handler.postDelayed(calculatingRunnable,calcDelay);

        lastAngle = 0;
        calculatedPercentage = 0;
        currentCalculatedPercentage = 0;
        counter = 0;
    }

    private Runnable calculatingRunnable = new Runnable() {

        @Override
        public void run() {

            currentCalculatedPercentage = differenceToPercentage(currentAngle);

            if(currentCalculatedPercentage >= calculatedPercentage){
                calculatedPercentage = currentCalculatedPercentage;
            }
            else{
                calculatedPercentage = 0;
            }

            lastAngle = currentAngle;
            handler.postDelayed(this,calcDelay);
        }
    };


    private int calculatePercentage(double angle){

        currentAngle = angle;
        Log.d(TAG, "calculatedPercentage = " + calculatedPercentage);
        return calculatedPercentage;
    }

    public float getCalculatedPercentageForAlpha(double angle){
        return calculatePercentage(angle) / 100f;
    }
}
